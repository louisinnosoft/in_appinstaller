﻿var UserInfo = ['<xml>',
              '<UserInfo>',
              '</UserInfo>',
              '</xml>'].join("");

var UserInfoTemp = ['<xml>',
              '<UserInfo>',
              '</UserInfo>',
              '</xml>'].join("");

var url = "";
var dialog;
var salt = "";
var lang = ""; //在login頁面時抓取val,再帶入
var default_lang = "";
var DoLogOut = "";
var UI_Resources;

function main_menu_click() {

}

//$(document).ajaxStart(function () {
//    debugger;
//    $.mobile.loading('show', {
//        theme: "b",
//        text: "Loading...",
//        textonly: false,
//        textVisible: false
//    });

//});
//$(document).ajaxComplete(function () {
//    $.mobile.loading('hide');
//});

function login() {
    //SaveConnInfo();
    //window.history.back(-1);
    var db = $("#db").val();
    var loginid = $("#loginid").val();
    var pwd = $("#pwd").val();
    var LoginOK = "false";
    lang = $("#lang").val();

    SaveConnInfo(db, loginid, pwd, lang);
    SetGlobalConnInfo();

    debugger;
    DoLogOut = localStorage.getItem("DoLogOut");

    if (DoLogOut != "true") {
        refreshtemplate(); //與DB進行Connect 
    } else {
        refreshtemplate();
    }
    //$.mobile.changePage('index.aspx'); 
  
}

function SetGlobalConnInfo() {
    url = localStorage.getItem("url");

    var loginid = localStorage.getItem("loginid");
    var db = localStorage.getItem("db");
    var pwd = localStorage.getItem("pwd");
    var clienttemplates = localStorage.getItem("clienttemplates");
    lang = localStorage.getItem("lang");
    //lang = "zc";

    var LoginOK = localStorage.getItem("LoginOK")

    if (!db || !loginid || !pwd)//若三者之一有空值，則強行跳至login.aspx
    {
        //$.mobile.changePage('login.aspx', { transition: 'pop', role: 'dialog' });
    }

    var xmlUserInfo = $(UserInfoTemp);
    var UserInfoElement = xmlUserInfo.find('UserInfo');
    $(UserInfoElement).append("<loginid>" + loginid + "</loginid>");
    $(UserInfoElement).append("<db>" + db + "</db>");
    $(UserInfoElement).append("<pwd>" + pwd + "</pwd>");
    $(UserInfoElement).append("<clienttemplates>" + clienttemplates + "</clienttemplates>");
    $(UserInfoElement).append("<lang>" + lang + "</lang>");

    UserInfo = xmlUserInfo.html();

}

function GetUrlParameter(PName) {
    var url = window.location.toString(); //取得當前網址
    var str = ""; //參數中等號左邊的值
    var str_value_db = ""; //參數中等號右邊的值: db

    if (url.indexOf("?") != -1) {
        //如果網址有"?"符號
        var ary = url.split("?")[1].split("&");
        //取得"?"右邊網址後利用"&"分割字串存入ary陣列 ["a=1","b=2","c=3"]

        for (var i in ary) {
            //取得陣列長度去跑迴圈，如:網址有三個參數，則會跑三次

            str = ary[i].split("=")[0];
            //取得參數"="左邊的值存入str變數中
            if (str == PName) {
                //若str等於想要抓取參數 如:b
                str_value_db = decodeURI(ary[i].split("=")[1]);
                //取得b等號右邊的值並經過中文轉碼後存入str_value
                //alert(str_value_db); //check取值結果用
                return str_value_db;
            } //if(str == PName)判斷是否有抓到帶入值，結束
        } //for迴圈結束
    } //if 判斷後方是否有帶參數，結束

    else {
        return "";
    }

}


function SaveConnInfo(db, loginid, pwd, t_lang) {
    debugger;
    url = location.protocol + "//" + location.hostname;
    if (location.port) {
        url += ":" + location.port;
    }
    url += "/bcsapp/bcsapp.ashx";
    localStorage.setItem("url", url);

    //判斷傳入值db, loginid, pwd()是否為空值，空值則不存入暫存；""則會傳入空字串 
    if (db && db != "" && db != undefined) {
        localStorage.setItem("db", db);
    }

    if (loginid && loginid != "" && loginid != undefined) {
        localStorage.setItem("loginid", loginid);
    }

    if (pwd && pwd != "" && pwd != undefined) {
        localStorage.setItem("pwd", pwd);
    }

    if (t_lang && t_lang != "" && t_lang != undefined) {
        localStorage.setItem("lang", t_lang);
        lang = t_lang;
    }
    
    //將LoginOK改為false
    var LoginOK = "false";
    localStorage.setItem("LoginOK", LoginOK);
   
    //SetGlobalConnInfo(); 
}

function LoadConnInfo() {
    debugger;
    var url = localStorage.getItem("url");
    var loginid = localStorage.getItem("loginid");
    var db = localStorage.getItem("db");
    var pwd = localStorage.getItem("pwd");
    lang = localStorage.getItem("lang");
 


    if (url) {
        $("#url").val(url);
        $("#db").val(db);
        $("#loginid").val(loginid);
        $("#pwd").val(pwd);
        //$("#lang").val(lang);
        $("#lang option[value=" + lang + "]").prop("selected", true);
        //SetGlobalConnInfo();
    }

    //alert(url);

}

function showConnInfo() {
    alert(url);
    alert(UserInfo);
}

function ShowLoginDialog() {
    $("#popupLogin").show();
}

function LoadLoginPage() {
    debugger;
    $.mobile.changePage('login.aspx', { reloadPage: true });
}


function hideAddressBar() {
    if (!window.location.hash) {
        if (document.height < window.outerHeight) {
            document.body.style.height = (window.outerHeight + 50) + 'px';
        }

        setTimeout(function () { window.scrollTo(0, 1); }, 50);
    }
}

function GetMyActivity() {
    try {
        debugger;

        var filter = "";
        $.ajax({
            url: url,
            type: "post",
            async: false,
            data: $.param({ 'method': 'bcs_GetMyActivity',
                'UserInfo': UserInfo
            }),
            dataType: "xml",
            success: function (data) {


                debugger;

                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status == "false") {
                    alert(result);
                    return;
                }
                else {
                    $.mobile.loading('show', {
                        theme: "b",
                        text: "Loading...",
                        textonly: false,
                        textVisible: false
                    });
                    //sherry-----------
                    //var temp_li = $("#temp_li").html();    

                    //Form_Name: MyActivity            
                    UpdateTemplate($(data).find("meta"));
                    var templatename = $(data).find("template_name").text();
                    var template = localStorage.getItem(templatename);
                    //sherry-----------

                    /*<h3>{#workflow_name}</h3>
                    <p><b>{#activity}</b></p>
                    <p class="ui-li-aside">{#stard_date}</p>
                    */
                    $(data).find("Item").each(function () {
                        debugger;                        
                        var lihtml = MapTempAndData3(template, $(this)); //1222：一開始傳入便已是aspx
                        var li = document.createElement('li');
                        $(li).html(lihtml)
                        .appendTo($("#MyActivity")) //parent ul
                    });
                    $('#MyActivity').listview('refresh');
                    $.mobile.loading('hide');
                }


            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }

}

function GetMyActivity2() {
    try {
        debugger;

        var filter = "";
        $.ajax({
            url: url,
            type: "post",
            async: false,
            data: $.param({ 'method': 'bcs_GetMyActivity2',
                'UserInfo': UserInfo
            }),
            dataType: "xml",
            success: function (data) {
                debugger;

                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status != "true") {
                    alert(result);
                    return;
                }
                else {

                    //sherry----------------
                    //var temp_li = $("#temp_li").html();

                    //Form_Name: MyActivity2
                    UpdateTemplate($(data).find("meta"));
                    var templatename = $(data).find("template_name").text();
                    var template = localStorage.getItem(templatename);

                    //sherry-----------------

                    /*<h3>{#workflow_name}</h3>
                    <p><b>{#activity}</b></p>
                    <p class="ui-li-aside">{#stard_date}</p>
                    */
                    $(data).find("Item").each(function () {
                        debugger;
                        var lihtml = MapTempAndData3(template, $(this));
                        var li = document.createElement('li');
                        $(li).html(lihtml)
                        .appendTo($("#MyActivity2")) //parent ul
                    });
                    $('#MyActivity2').listview('refresh');
                }
            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }

}



function MapTempAndData3(temp, data_record, data_header) {

    debugger;
    //data_record 與 data_header 同時傳入,代表該Template是同時包含Record與Header
    //先處理表頭
    $(data_header).children().each(function () {
        var tagname = $(this).prop("tagName").toLowerCase();
        var tagvalue = $(this).text();
        var regexp = new RegExp("{#" + tagname + "}", 'g');
        temp = temp.replace(regexp, tagvalue);
    });    

    debugger;
    //處理表身
    $(data_record).children().each(function () {
        debugger;
        //var tagname = $(this).prop("tagName").toLowerCase();
        var tagname = $(this).prop("tagName");
        var tagvalue = $(this).text(); //最原始的值,可能是ID
        var taglabel = $(this).attr("label");
        taglabel = (!taglabel) ? tagvalue : taglabel; //人看的值
        var datatype = $(this).attr("datatype");

        var str_r = taglabel;

        switch (datatype) {
            case "item":
                var itemtype = $(this).attr("itemtype");
                var itemid = $(this).attr("itemid");
                switch (itemtype) {
                    case "User":
                    case "Identity":
                        var tel = $(this).attr("tel");
                        var email = $(this).attr("email");
                        str_r = "<a href='#'  class='PopupUserMenu float-left ui-btn ui-btn-inline ui-icon-user ui-btn-icon-left ui-corner-all'";
                        str_r += " data-position-to='window' data-rel='popup' data-transition='flip'";
                        if (tel) {
                            str_r += " tel='" + tel + "'";
                        }
                        if (email) {
                            str_r += " email='" + email + "'";
                        }
                        str_r += "></a>";
                        str_r += "<div class='float-left'>" + taglabel + "</div>";
                        if (taglabel.length == 0)
                            str_r = "";
                        break;
                    case "File":
                        var file_size = $(this).attr("file_size");
                        debugger;
                        if (file_size) {
                            str_r = "<div id='" + itemid + "_move'>";
                            str_r += "<a href='#'  class='float-left ui-btn ui-btn-inline ui-icon-cloud ui-btn-icon-top ui-btn-icon-notext ui-corner-all' onclick=\"DownloadFile('" + itemid + "')\" ></a>";
                            str_r += "<div class='float-left filelabel'>" + taglabel + "(" + file_size + "kb)</div>";
                            str_r += "</div>";
                            str_r += "<img src='../Images/progressBar.gif' id='" + itemid + "_img' style='display:none' />";
                            str_r += "<div id='" + itemid + "_download' style='display:none'>";
                            str_r += "<a href='" + url.replace("bcsapp.ashx", "tempvault/") + itemid + "/" + taglabel + "'   class='float-left ui-btn ui-btn-inline ui-icon-action ui-btn-icon-top ui-btn-icon-notext ui-corner-all' data-ajax='false'></a>";
                            str_r += "<div class='float-left filelabel'>" + taglabel + "(" + file_size + "kb)</div>";
                            str_r += "</div>";
                        }
                        else {
                        }

                        break;
                    default:
                        var extend_class = "";
                        if (tagname == "related_id_value" || tagname == "id_value") {
                            extend_class = " ui-btn ui-icon-carat-r ui-btn-icon-notext ui-corner-all";
                        }
                        str_r = "<a href='Item.htm?itemtype=" + $(this).attr("itemtype") + "&amp;itemid=" + $(this).attr("itemid") + "&amp;lang={#lang}&amp;t={#salt}'  class='float-left" + extend_class + "'>" + taglabel + "</a>";
                }
                break;
            case "boolean":
                if (tagvalue == "1") {
                    str_r = "<input type='checkbox' checked='true'  onclick='return false;' />";
                }
                else {
                    str_r = "<input type='checkbox' onclick='return false;' />";
                }
                break;
            case "color list":
            case "color":
                str_r = "<div style=padding:5px;background-color:" + tagvalue + ";/>";
                break;           
            default:
                if (datatype == undefined) {

                }
                else {
                    var SupportFormat = "boolean,string,text,integer,float,decimal,date,sequence,item,list,filter list,color,color list,foreign";
                    var TryInteger = SupportFormat.indexOf(datatype);
                    if (TryInteger == "-1") {
                        if (tagvalue != "") {
                            str_r = "不支援的格式";
                        } else {
                            str_r = "無物件";
                        }
                    }
                    debugger;
                }
                break;


        }





        debugger;


        var regexp = new RegExp("{#" + tagname + "_f}", 'g');
        temp = temp.replace(regexp, str_r);

        var regexp = new RegExp("{#" + tagname + "}", 'g');
        temp = temp.replace(regexp, tagvalue);




    });

    var regSalt = new RegExp("{#salt}", 'g');
    temp = temp.replace(regSalt, salt);

    var reglang = new RegExp("{#lang}", 'g');
    temp = temp.replace(reglang, lang);

    if (true) {
        //移除沒被置換到的多餘欄位
        debugger;
        temp = "<temp>" + temp + "</temp>";
        var xmlDoc;
        try {
            console.log("temp:" + temp);
            xmlDoc = $.parseXML(temp);
        }
        catch (e) {
            alert(e.message);
            return;
        }
        var $xml = $(xmlDoc);
        //var $xml = $(temp);
        //remove 可以參考這一篇: http://stackoverflow.com/questions/8065775/remove-xml-node-with-jquery-each-and-this


        $xml.find(".tempfield:contains('{#')").each(function () {
            $(this).closest('.tempblock').remove();
        });


        //處理Template內的多語系
        $xml.find(".multilang").each(function () {
            var l10nValue = $(this).attr(lang);
            $(this).html(l10nValue);
        });

        //處理Relationship上的File下載,只保留下載按鈕,不要有檔案名稱
        $xml.find(".temp_rel_content>div>.filelabel").remove();

        temp = $xml.find("temp").html();
    }
    return temp;
}


function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function getPageParameter(pageitem, sParam) {
    var sPageURL = $(pageitem).data("url").split("?")[1];
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function DownloadFile(fileid) {
    try {
        debugger;
        FileControlsHandler(fileid, "img");


        var filter = "";
        $.ajax({
            url: url,
            type: "post",
            async: true,
            data: $.param({ 'method': 'GetFile',
                'UserInfo': UserInfo,
                'criteria': fileid
            }),
            dataType: "xml",
            success: function (data) {
                debugger;

                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status != "true") {
                    alert(result);
                    return;
                }
                else {
                    var fileid = $(data).find("fileid").text();
                    FileControlsHandler(fileid, "download");
                    $("#DownloadFileUrl").attr("href", $("#" + fileid + "_download>a").attr("href"));
                    $("#DownloadFileUrl").html("下載<br/>" + $(data).find("filename").text() + "(" + $(data).find("file_size").text() + "kb)");
                    //$("#DialogDownloadFile").popup();
                    $("#DialogDownloadFile").popup("open");
                }
            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }
}


function ViewSearchResult(criteria) {
    try {
        //debugger;
        var filter = "";
        var currentdate1 = new Date();
        var tmplog = $("#timelog").html();
        tmplog += "startcall:" + currentdate1.toLocaleTimeString() + "<br/>";
        $("#timelog").html(tmplog);
        console.log("startcall:" + currentdate1.toLocaleTimeString());
        //ShowProgressBarAuto(10);
        $.ajax({
            url: url,
            type: "post",
            async: true,
            data: $.param({ 'method': 'bcs_GetSearchResult',
                'UserInfo': UserInfo,
                'criteria': criteria
            }),
            dataType: "xml",
            success: function (data) {
                //debugger;
                $.mobile.loading('show', {
                    theme: "b",
                    text: "Loading...",
                    textonly: false,
                    textVisible: false
                });
                //HideProgressBar();
                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status == "false") {
                    alert(result);
                    return;
                }
                else {
                    debugger;
                    $.mobile.loading('show', {
                        theme: "b",
                        text: "Loading...",
                        textonly: false,
                        textVisible: false
                    });

                    $('#SearchResult').children().remove();

                    //                    var templatename = $(data).find("template_name").text();
                    //                    var temp_li = localStorage.getItem(templatename);

                    UpdateTemplate($(data).find("meta"));
                    var templatename = $(data).find("template_name").text();
                    var temp_li = localStorage.getItem(templatename);

                    var header_li = localStorage.getItem(templatename + "_header");
                    if (!header_li) {
                        header_li = header_li.replace(/\_value/g, '_label');
                    }

                    header_li = MapTempAndData3(header_li, $(data).find("Header"));
                    var li = document.createElement('li');
                    $(li).html(header_li)
                        .appendTo($("#SearchResult")) //parent ul

                    /*<h3>{#workflow_name}</h3>
                    <p><b>{#activity}</b></p>
                    <p class="ui-li-aside">{#stard_date}</p>
                    */
                    $(data).find("Item").each(function () {
                        debugger;
                        var lihtml = MapTempAndData3(temp_li, $(this));
                        var li = document.createElement('li');
                        $(li).html(lihtml)
                        .appendTo($("#SearchResult")) //parent ul
                    });
                    $('#SearchResult').listview('refresh');
                    $.mobile.loading('hide');
                    

                }


            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }
}


function ViewItem(criteria) {
    try {
        debugger;
        var filter = "";
        $.ajax({
            url: url,
            type: "post",
            async: true,
            data: $.param({ 'method': 'bcs_GetItem',
                'UserInfo': UserInfo,
                'criteria': criteria
            }),
            dataType: "xml",
            success: function (data) {
                debugger;

                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status == "false") {
                    alert(result);
                    return;
                }
                else {
                    debugger;

                    UpdateTemplate($(data).find("meta"));
                    var templatename = $(data).find("template_name").text();
                    var temp_item_property = localStorage.getItem(templatename);

                    var itemtype = $(data).find("itemtype").text();
                    var itemid = $(data).find("itemid").text();
                    //var itemtypeName = $(data).find("itemtypeName").text() //1216取itemtypeName
                    var itemtypeLabel = $(data).find("itemtypelabel").text() //1217取itemtypeLabel

                    $("#ItemProfile").attr("itemtype", itemtype);
                    $("#ItemProfile").attr("itemid", itemid);
                    //$("#itemtypeName").html(itemtypeName); //1216放itemtypeName至span //1217註解
                    $("#itemtypelabel").html(itemtypeLabel); //1217放itemtypeName至span 

                    debugger;
                    var lihtml = MapTempAndData3(temp_item_property, $(data).find("Item"), $(data).find("Header"));

                    var li = document.createElement('div');
                    $(li).html(lihtml)
                        .appendTo($("#ItemProfile"))



                    //                    $(data).find("Item").each(function () {
                    //                        debugger;
                    //                        var lihtml = MapTempAndData3(temp_item_property, $(this), $(data).find("Header"));
                    //                        var li = document.createElement('div');
                    //                        $(li).html(lihtml)
                    //                            .appendTo($("#ItemProfile"))
                    //                    });


                    var temp_rel_tab_form = $("#temp_rel_tab_form").html();
                    var temp_rel_tab_page = $("#temp_rel_tab_page").html();





                    $(data).find("RelationshipType").children().each(function () {
                        debugger;
                        var RelForm = $(this).find("form").text();
                        var RelStart_Page = $(this).find("start_page").text();
                        var RelParameters = $(this).find("Parameters").text();

                        if (RelStart_Page.length > 1) {
                            var lihtml = MapTempAndData3(temp_rel_tab_page, $(this));
                            var li = document.createElement('div');
                            $(li).html(lihtml)
                                 .appendTo($("#ItemContainer"))
                                 .addClass("float-clear rel_tab")
                                 .attr("itemtype", $(this).find("name").text());
                        }
                        else {
                            var lihtml = MapTempAndData3(temp_rel_tab_form, $(this));
                            var li = document.createElement('div');
                            $(li).html(lihtml)
                                .appendTo($("#ItemContainer"))
                                .attr("data-role", "collapsible")
                                .addClass("float-clear rel_tab")
                                .attr("itemtype", $(this).find("name").text());
                        }

                    });


                    //ActionButton
                    $(data).find("Actions").children().each(function () {
                        debugger;
                        var ActionButtonHtml = "<a href='{@url}' data-role='button' >{@label}</a>";
                        var a_ActionButton = document.createElement('a');
                        $(a_ActionButton).attr("href", $(this).find("url").text())
                                        .html($(this).find("label").text())
                                        .attr("data-role", "button")
                                        .attr("data-icon", "arrow-u")
                                        .appendTo($("#ActionButtons"))
                                        .button();
                    });

                    $("#ItemContainer").trigger('create');
                    BindingEvents();
                    $(".rel_tab").on("collapsiblecollapse", function (event, ui) {
                        //debugger;
                        //alert($(event.target).attr("id") + "collapse");
                    });
                    $(".rel_tab").on("collapsibleexpand", function (event, ui) {
                        debugger;
                        //alert($(event.target).attr("id") + "expand");
                        var el = $(event.target);
                        ViewRel(el);
                    });

                }


            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }
}

function ViewRel2(target_div, methodname, criteria) {
    if ($(target_div).hasClass("loaded")) {
        return;
    }

    try {
        debugger;
        var filter = "";
        $.ajax({
            url: url,
            type: "post",
            async: false,
            data: $.param({ 'method': methodname,
                'UserInfo': UserInfo,
                'criteria': criteria
            }),
            dataType: "xml",
            success: function (data) {
                debugger;

                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status == "false") {
                    alert(result);
                    return;
                }
                else {
                    debugger;
                    var TargetUL = $(target_div).find("ul");

                    UpdateTemplate($(data).find("meta"));
                    var templatename = $(data).find("template_name").text();
                    var tmpTaskLi = localStorage.getItem(templatename);


                    var templatename = $(data).find("template_name").text();
                    var itemtype = $(data).find("itemtype").text();
                    var temp_rel = localStorage.getItem(templatename);



                    var tempxml = $.parseXML("<temp>" + temp_rel + "</temp>");
                    var $xml = $(tempxml);
                    if ($xml.find("temp>table").length>0) {
                        //代表是Table形式
                        //<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
                        //<thead>
                        //<tr>
                        //  <th  class='float-left temp_rel_content tempfield tempblock' data-priority="1">{#p1_label}</th>
                        //</tr>
                        //</thead>
                        //<tbody>
                        //<tr>
                        //  <td class='float-left temp_rel_content tempfield tempblock'>{#p1_value_f}</td>
                        //</tr>	                
                        //</tbody>
                        //</table>
                        var temp_tbody = $(temp_rel).find("tbody").html();
                        var temp_thead = $(temp_rel).find("thead").html();
                        $xml.find("tbody").children().remove();
                        $xml.find("thead").children().remove();
                        temp_thead = MapTempAndData3(temp_thead, $(data).find("Header"));
                        $xml.find("thead").append(temp_thead);

                        $(data).find("Item").each(function () {
                            debugger;
                            var tbody = MapTempAndData3(temp_tbody, $(this));
                            $xml.find("tbody").append(tbody);
                        });


                        var li = document.createElement('li');
                        $(li).html($xml.html())
                             .appendTo(TargetUL) //parent ul

                        $("#table-column-toggle").trigger("create");
                        $("#table-column-toggle").table();

                    }
                    else {
                        //代表是 div 的形式
                        //<div class='float-left temp_rel_content tempfield tempblock'>{#p1_value_f}</div>

                        var header_li = localStorage.getItem(templatename + "_header");
                        if (!header_li) {
                            header_li = temp_rel.replace(/\_value_f/g, '_label');
                            header_li = header_li.replace(/\_value/g, '_label');
                        }
                        header_li = MapTempAndData3(header_li, $(data).find("Header"));
                        var li = document.createElement('li');
                        $(li).html(header_li)
                             .appendTo(TargetUL) //parent ul

                        $(data).find("Item").each(function () {
                            debugger;

                            temp_rel_record1 = MapTempAndData3(temp_rel, $(this));
                            var rel_record = document.createElement('li');
                            $(rel_record).html(temp_rel_record1)
                                      .appendTo(TargetUL)
                                      .addClass("float-clear");
                        });

                    }

                    $(TargetUL).trigger('create');
                    $(target_div).addClass('loaded');
                    BindingEvents();
                }
            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }
}



function ViewRel(target_div) {
    if ($(target_div).hasClass("loaded")) {
        return;
    }
    var source_id = $("#ItemProfile").attr("itemid");
    var Rel_Name = $(target_div).attr("itemtype");
    
    var regBlank = new RegExp(" ", 'g');
    var str_where = "[" + Rel_Name.replace(regBlank, "_") + "].[source_id]='" + source_id + "'";
    var criteria = "itemtype:" + Rel_Name + "&where:" + str_where + "&app:bcs_app3";

    ViewRel2(target_div, "bcs_GetRel", criteria);
}

function ReplaceBlank(v) {
    if (v) {
        v = v.replace(" ", "_");
    }
    return v;
}

function UrlParameterHandler_old() { //取Url參數值比對項目是否正確

    //取新值
    var db = GetUrlParameter('db');
    var loginid = GetUrlParameter('loginid');
    var pwd = GetUrlParameter('pwd');

    //取原Url參數
    var loginid_original = localStorage.getItem("loginid");
    var db_original = localStorage.getItem("db");
    var pwd_original = localStorage.getItem("pwd");
    lang = localStorage.getItem("lang"); //1218取localstorge中的lang


    //不同帳號，且依訂有輸入密碼時
    if (loginid != loginid_original && pwd != pwd_original && pwd != "" && pwd != undefined) //比對Url_id參數，若不同且pwd非空值(避免重新跳轉的index頁面)
    {
        //將新的db、login、pwd 都存入資料庫
        SaveConnInfo(db, loginid, pwd); //三個參數都有值，存到資料庫
    }

    //避免不同帳號同密的問題
    if (loginid != loginid_original && loginid != "" && (pwd == "" || pwd == undefined)) //比對Url_id參數，若不同且pwd非空值(避免重新跳轉的index頁面)
    {
        pwd = "aaa";
        //將新的db、login、pwd 都存入資料庫
        SaveConnInfo(db, loginid, pwd); //三個參數都有值，存到資料庫
    }

    //    //同帳號而不帶密碼參數
    //    if (loginid == loginid_original && (pwd == "" || pwd == undefined )) {
    //        localStorage.setItem("pwd", pwd_original); //重新抓取上次登入的pwd
    //        SaveConnInfo(db_original, loginid_original, pwd_original); //三個參數都有值，存到資料庫
    //    }

    var LoginOK = localStorage.getItem("LoginOK")

    if (loginid == "" && pwd == "" && (LoginOK == null || LoginOK != "false")) {

        //若帳號和密碼都沒帶到參數，且LoginOK為null，表第一次登入，不進行refreshtemplate
        $("#db").val(db_original);
        $("#loginid").val(loginid_original);
        $("#pwd").val(pwd_original);

        SaveConnInfo(db_original, loginid_original, pwd_original);
    }
    
   
    if (LoginOK == "false") {
        refreshtemplate(); //與DB進行Connect 
    }
}

function UrlParameterHandler() { //取Url參數值比對項目是否正確

    debugger;
    //取新值
    var db = GetUrlParameter('db');
    var loginid = GetUrlParameter('loginid');
    var pwd = GetUrlParameter('pwd');
    var t_lang = GetUrlParameter('lang');

    //取原Url參數
    var loginid_original = localStorage.getItem("loginid");
    var db_original = localStorage.getItem("db");
    var pwd_original = localStorage.getItem("pwd");
    var lang_original = localStorage.getItem("lang"); //1218取localstorge中的lang

    loginid_original = (loginid_original) ? loginid_original : "";
    db_original = (db_original) ? db_original : "";
    pwd_original = (pwd_original) ? pwd_original : "";
    lang_original = (lang_original) ? lang_original : default_lang;

    db = (db) ? db : db_original;
    loginid = (loginid) ? loginid : loginid_original;
    pwd = (pwd) ? pwd : pwd_original;
    t_lang = (t_lang) ? t_lang : lang_original;



    //不同帳號，且依訂有輸入密碼時
    if (loginid != loginid_original) //比對Url_id參數，若不同且pwd非空值(避免重新跳轉的index頁面)
    {
        //將新的db、login、pwd 都存入資料庫
        pwd = "";
    }

    
    SaveConnInfo(db, loginid, pwd, t_lang); //三個參數都有值，存到資料庫

    var LoginOK = localStorage.getItem("LoginOK");

    DoLogOut = localStorage.getItem("DoLogOut");
    if (LoginOK == "false" && DoLogOut == "false") {
        refreshtemplate(); //與DB進行Connect 
    }
}
//Page Show Handler

$(document).on("pageshow", "#ViewMyActivity", function () {
    debugger;
    //SetGlobalConnInfo(); 
    // GetAlll10nValue($("#MyActivity")); //1218整合到bcs_InitPage中
    //GetAlll10nValue($("#appheader"));
    //GetAlll10nValue($("#ViewMyActivity")); //1218為測試MyActivity多語系抓child，成功
    
    debugger;
    bcs_InitPage($(this).attr("id"));
    GetMyActivity();
});

$(document).on("pageshow", "#ViewMyActivity2", function () {
    debugger;
    //SetGlobalConnInfo(); 
    //GetAlll10nValue($("#appheader"));//1218整合到bcs_InitPage中
   
   bcs_InitPage($(this).attr("id"));
   GetMyActivity2();
});


$(document).on("pageshow", "#login", function () {
    debugger;

    UrlParameterHandler();
    //SetGlobalConnInfo();
    //GetAlll10nValue($("#appheader"));//1218整合到bcs_InitPage中
    LoadConnInfo();
    bcs_InitPage($(this).attr("id"));

});

$(document).on("pageshow", "#default", function () {
    debugger;

    //取Url參數，與DB中資料進行比對
    //UrlParameterHandler();
    SetGlobalConnInfo();
    GetAlll10nValue($("#default"));
    // GetAlll10nValue($("#appheader"));//1218整合到bcs_InitPage中
    bcs_InitPage($(this).attr("id"));
    DoLogOut = "false";

});




$(document).on("pageshow", "#ViewSearchResult", function () {
    debugger;
    // GetAlll10nValue($("#appheader"));//1218整合到bcs_InitPage中
    bcs_InitPage($(this).attr("id"));

    //SetGlobalConnInfo();
    //Get Criteria
    var criteria = localStorage.getItem("[ViewSearchResult][criteria]");
    criteria = "itemtype:project&where:[project].[state]='active'&top:30";
    if (criteria == "") {
        return;
    }

    //creteria
    /*
    itemtype:project&where:[project].[state]='active'
    */
    bcs_InitPage($(this).attr("id"));

    ViewSearchResult(criteria);
    localStorage.setItem("[ViewSearchResult][criteria]", "");

});

$(document).on("pageshow", "#ViewItem", function () {
    debugger;
    GetAlll10nValue($("#appheader"));
    //SetGlobalConnInfo();
    //Get Criteria
    var criteria = localStorage.getItem("[ViewItem][criteria]");

  

    var itemtype = getPageParameter($(this), "itemtype");
    var itemid = getPageParameter($(this), "itemid");
    //var itemtype = getUrlParameter("itemtype");
    //var itemid = getUrlParameter("itemid");

    bcs_InitPage($(this).attr("id"));

    criteria = "itemtype:" + itemtype + "&itemid:" + itemid + "&app:bcs_app2";
    if (criteria == "") {
        return;
    }

    //creteria
    /*
    itemtype:project&id:3852CE3CFAB447FFAB61343AB1951676&app:app2
    */

    ViewItem(criteria);
    localStorage.setItem("[ViewItem][criteria]", "");
});

function showDialog() {

    debugger;

    $.mobile.changePage('Default.htm', { transition: 'pop', role: 'dialog' });
}

function showProcess() {
    $('<input>').appendTo('#downloading').attr({ 'name': 'slider', 'id': 'slider', 'data-highlight': 'true', 'min': '0', 'max': '100', 'value': '50', 'type': 'range' }).slider({
        create: function (event, ui) {
            $(this).parent().find('input').hide();
            $(this).parent().find('input').css('margin-left', '-9999px'); // Fix for some FF versions
            $(this).parent().find('.ui-slider-track').css('margin', '0 15px 0 15px');
            $(this).parent().find('.ui-slider-handle').hide();
        }
    }).slider("refresh");

    // Test
    var i = 1;
    var interval = setInterval(function () {
        progressBar.setValue('#slider', i);
        if (i === 100) {
            clearInterval(interval);
        }
        i++;
    }, 100);
}

function refreshtemplate() {
    try {
        debugger;
        var filter = "";
        //SaveConnInfo();
        SetGlobalConnInfo();
        var clienttemplates = localStorage.getItem("clienttemplates");
        var criteria = "clienttemplates:" + clienttemplates;
        $.ajax({
            url: url,
            type: "post",
            async: false,
            data: $.param({ 'method': 'bcs_GetTemplates',
                'UserInfo': UserInfo,
                'criteria': criteria
            }),
            dataType: "xml",
            success: function (data) {
                debugger;

                var status = $(data).find("status").text();
                var result = $(data).find("result").text();
                if (status == "false") {
                    alert(result);
                    //$.mobile.changePage('login.aspx', { transition: 'pop', role: 'dialog' });
                    //$.mobile.changePage('login.aspx');
                    return;
                }
                else {
                    debugger;


                    var templatenames = $(data).find("templatenames").text();
                    localStorage.setItem("clienttemplates", templatenames);

                    $(data).find("template").each(function () {
                        debugger;
                        var template_name = $(this).find("name").text();
                        var bcs_template = $(this).find("bcs_template").text();
                        localStorage.setItem(template_name, bcs_template);

                        var bcs_template_header = $(this).find("bcs_template_header").text();
                        if (bcs_template_header.length > 0) {
                            localStorage.setItem(template_name + "_header", bcs_template_header);
                        }


                    });

                    var loginid = localStorage.getItem("loginid");

                    alert(loginid + "Update Ok"); //第一次，一定都會取到值整成html拋回，成功則Update Ok
                    var LoginOK = "true"
                    localStorage.setItem("LoginOK", LoginOK);

                    DoLogOut = "false";
                    localStorage.setItem("DoLogOut", DoLogOut); //登入後將因登出而鎖住的阻斷拿掉
                    $.mobile.changePage('index.aspx'); //並於第一次Update Ok後強制跳轉到index，並不帶任何參數

                }


            }
        });
    }
    catch (ex) {
        //HideProgressAnimation();
        debugger;
        alert(ex.message);
    }
}

function ConvertToArasDateTime(thedate) {
    var dd = thedate.getDate();
    var mm = thedate.getMonth() + 1; //January is 0!
    var yyyy = thedate.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    var h = thedate.getHours();
    var m = thedate.getMinutes();
    var s = thedate.getSeconds();

    if (h < 10) {
        h = '0' + h;
    }

    if (m < 10) {
        m = '0' + m;
    }

    if (s < 10) {
        s = '0' + s;
    }

    rdate = yyyy + "-" + mm + '-' + dd + "T" + h + ":" + m + ":" + s;

    return rdate;
}



function FileControlsHandler(fileid, ShowElement) {
    $("#" + fileid + "_move").hide();
    $("#" + fileid + "_img").hide();
    $("#" + fileid + "_download").hide();

    $("#" + fileid + "_" + ShowElement).show();
}

function UpdateTemplate(MetaElement) {
    //<meta>";
    //<template_name>" + TemplateName + "</template_name>";
    //<templatenames><![CDATA[" + NewClientTemplateNames + "]]></templatenames>";
    //<template>
    //<name></name>
    //<bcs_template><![CDATA[]]></bcs_template>
    //<bcs_template_header><![CDATA[]]></bcs_template_header>
    //</template>
    //</meta>";

    if ($(MetaElement).find("template>name").text().length <= 0)
        return;

    var ClientTemplateNames = $(MetaElement).find("templatenames").text();
    var TemplateName = $(MetaElement).find("name").text();
    var bcs_template = $(MetaElement).find("bcs_template").text();
    var bcs_template_header = $(MetaElement).find("bcs_template_header").text();

    localStorage.setItem(TemplateName, bcs_template);

    if (bcs_template_header.length > 0) {
        localStorage.setItem(TemplateName + "_header", bcs_template_header);
    }

    localStorage.setItem("clienttemplates", ClientTemplateNames);

}

function GetAlll10nValue(element) {
    $(element).find(".multilang").each(function () {
        //debugger;
        var key = $(this).attr("lang-key");
        var v = Getl10nValue(key);
        $(this).html(v);
    });
}

function Getl10nValue(key,DefaultValue) {
    LoadUI_Resources();
    //debugger;
    var r = $(UI_Resources).find("resource[key='" + key + "']>" + lang).text();
    if (!r || r.length==0)
        r = DefaultValue;
    return r;
}

function LoadUI_Resources() {
    if (UI_Resources) {
        return;
    }        
    var tmp = localStorage.getItem("bcs_ui_resources");
    UI_Resources = $.parseXML(tmp);
}

function bcs_InitPage(pageid) {
    //$("#ActionButtons").children().remove();

    GetAlll10nValue($("#" + pageid)); //1218測試值接將pageid帶入
}

function BindingEvents() {
    $(".PopupUserMenu").click(function (e) {
        debugger;
        $("#UserMenu").enhanceWithin().popup();
        e.preventDefault();
        var tel = $(this).attr("tel");
        $("#tel").attr("content", tel);
        $("#tel").click(function (e) {
            var tel = $("#tel").attr("content");
            window.parent.location.href = "tel:" + tel;
        });
        var email = $(this).attr("email");
        $("#email").attr("content", email);
        $("#email").click(function (e) {
            var email = $("#email").attr("content");
            window.parent.location.href = "mailto:" + email;
        });
        // Instantiate the popup on DOMReady, and enhance its contents
        $("#UserMenu").popup("open", { transition: "slideup" });
    });   
}

var pbar;
var $popUp;
function ShowProgressBar(max) {
    //create a div for the popup
    $popUp = $("<div/>").popup({
        dismissible: false,
        theme: "b",
        overlyaTheme: "e",
        id:"PBDialog",
        transition: "pop"
    }).on("popupafterclose", function () {
        //remove the popup when closing
        $(this).remove();
    }).css({
        'width': '270px',
        'height': '200px',
        'padding': '5px'
    });
    //create a title for the popup
    $("<h2/>", {
        text: "Header"
    }).appendTo($popUp);

    //create a message for the popup
    $("<p/>", {
        text: "Welcome!"
    }).appendTo($popUp);

    $("<div/>", {
        id: "progressbar"
    }).appendTo($popUp);
       
    $popUp.popup('open').trigger("create");

    pbar = jQMProgressBar('progressbar')
    .setOuterTheme('b')
    .setInnerTheme('e')
    .isMini(true)
    .setMax(max)
    .showCounter(true)
    .setInterval(10)
    .build();
}


function SetProgressBarValue(v) {
    pbar.setValue(v);
}

function ShowProgressBarAuto(interval) {




    $popUp = $("<div/>").popup({
        dismissible: false,
        theme: "b",
        overlyaTheme: "e",
        id: "PBDialog",
        transition: "pop"
    }).on("popupafterclose", function () {
        //remove the popup when closing
        $(this).remove();
    }).css({
        'width': '270px',
        'height': '200px',
        'padding': '5px'
    });
    //create a title for the popup
    $("<h2/>", {
        text: "Header"
    }).appendTo($popUp);

    //create a message for the popup
    $("<p/>", {
        text: "Welcome!"
    }).appendTo($popUp);

    $("<div/>", {
        id: "progressbar"
    }).appendTo($popUp);

    $popUp.popup('open').trigger("create");

    pbar = jQMProgressBar('progressbar')
    .setOuterTheme('b')
    .setInnerTheme('e')
    .isMini(true)
    .setMax(100)
    .showCounter(true)
    .setInterval(interval)
    .build()
    .run();

}

function HideProgressBar() {
    $popUp.popup('close').trigger("create");
}

function ShowProgressBar2() {
    debugger;
    ShowProgressBar(100);
   
//    SetProgressBarValue(10);
//    SetProgressBarValue(20);
//    SetProgressBarValue(30);
        var i;
        for (i = 0; i < 50;i++ ) {
            debugger;            
            SetProgressBarValue(i);
        }

        setTimeout(function () {
            SetProgressBarValue(70);
            $popUp.popup('close').trigger("create");
        }, 3000);




    }



var i = 0;
function ShowProgressBar3() {
    i++;
   SetProgressBarValue(i);
}

function StopLoginAction() {
    debugger;
    //將原本正確的帳號密碼存放入localStorge，讓login頁面可以進行比對、存放txt
    var loginid = localStorage.getItem("loginid");
    var db = localStorage.getItem("db");
    var pwd = localStorage.getItem("pwd");
    lang = localStorage.getItem("lang");

    var LoginOK = "True";
    localStorage.setItem("LoginOK", LoginOK);
    
    DoLogOut = "True"
    localStorage.setItem("DoLogOut", DoLogOut);
    
    $.mobile.changePage('login.aspx');
}

function GetOptionValue() {

    lang = $('#lang option:selected').val(); //將在login所選取到的多語系val存入lang
    localStorage.setItem('lang', lang);
    return lang;
}
