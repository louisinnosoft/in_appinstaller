﻿function showModal() {
    $("body").append('<div class="modalWindow"/>');
    $.mobile.showPageLoadingMsg();
}

function hideModal() {
    $(".modalWindow").remove();
    $.mobile.hidePageLoadingMsg();
}