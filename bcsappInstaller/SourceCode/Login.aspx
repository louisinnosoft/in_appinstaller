﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="bcsappweb.Login" %>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="mobile-web-app-capable" content="yes">

<head>
   <script src="Scripts/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="Scripts/ajaxbusy.js" type="text/javascript"></script>
    <link href="Scripts/jquery.mobile-1.4.4/jquery.mobile-1.4.4.min.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/default.css?t=<%=salt%>" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery.mobile-1.4.4/jquery.mobile-1.4.4.min.js" type="text/javascript"></script>
    <script src="Scripts/bcsapp.js?t=<%=salt%>" type="text/javascript"></script>
    <link rel="stylesheet" href="themes_1106/1106_LVTest.min.css" />
    <link rel="stylesheet" href="themes_1106/jquery.mobile.icons.min.css" />
    
    <title></title>
    <script>
        $(function () {
            debugger;
            LoadConnInfo();
        });
        window.addEventListener("load", function () { if (!window.pageYOffset) { hideAddressBar(); } });
        window.addEventListener("orientationchange", hideAddressBar);

        
        salt = '<%=salt %>';
        default_lang = '<%=default_lang %>';

    </script>
</head>
<body>
    
    <div data-role="page" id="login">
      <div data-role="header" data-theme="c" id="appheader">
            <table width="100%">
              <tr>
                <td>
                   <div class="main-title multilang" lang-key="Common.AppTitle" >博威APP</div>
                </td>
                <td width="5%">
                   <!-- <button type="button" onclick="reloadiframe()">Reload</button>-->
                </td>
        
                    <td width="5%" align="left">
                       <a href="MainMenu.htm?t=4" data-iconpos="notext" data-role="button" data-icon="gear"></a>
                         
                    </td>
                </tr>
            </table>
        </div>
        <div class="main-content">
	     <label for="lang"> <span class="multilang" lang-key="Login.SelectLanguage" >選擇語言</span></label>
        <select name="lang" id="lang">
        <option value="zt">繁體中文</option>
        <option value="zc">简体中文</option>
		 <option value="en">English</option>
        </select>
        
        <label for="db"><span class="multilang" lang-key="Login.DB" >資料庫</span>:</label>
        <input type="text" id="db" />

        <label for="loginid"><span class="multilang" lang-key="Login.ID" >帳號</span>:</label>
        <input type="text" id="loginid" />

        <label for="pwd"><span class="multilang" lang-key="Login.Pwd" >密碼</span>:</label>
        <input type="password" id="pwd" />
        
        <button type="button" onclick="login()"><span class="multilang" lang-key="Login.SignIn" >登入</span></button>
      
        </div>
        </div>
        <div data-role="popup" id="UserMenu" data-theme="d">
        <div data-role="controlgroup" data-type="horizontal">
            <a href="#" id="tel" data-role="button" data-icon="phone" data-theme="b" content="">
                phone</a> <a href="#" id="mail" data-role="button" data-icon="mail" data-theme="b"
                    content="">mail</a>
        </div>
    </div>
   
</body>
</html>