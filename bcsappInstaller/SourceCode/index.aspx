﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="bcsappweb.index" %>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="mobile-web-app-capable" content="yes">
<head>
    <script src="Scripts/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="Scripts/ajaxbusy.js" type="text/javascript"></script>
    <link href="Scripts/jquery.mobile-1.4.4/jquery.mobile-1.4.4.min.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/default.css?t=<%=salt %>" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery.mobile-1.4.4/jquery.mobile-1.4.4.min.js" type="text/javascript"></script>
    <script src="Scripts/bcsapp.js?t=<%=salt %>" type="text/javascript"></script>
    <link rel="stylesheet" href="themes_1106/1106_LVTest.min.css" />
    <link rel="stylesheet" href="themes_1106/jquery.mobile.icons.min.css" />
    <script type="text/javascript" src="Scripts/jQMProgressBar/js/jQMProgressBar.js"></script>
    <link rel="stylesheet" type="text/css" href="Scripts/jQMProgressBar/css/jQMProgressBar.css" />
    <link href="Scripts/JQMBusy/JQMBusy.css" rel="stylesheet" type="text/css" />
    
    <title></title>
    
</head>
<body>
    
    <div id="default" data-role="page">
    

       <div data-role="header" data-theme="c" id="appheader">
            <table width="100%">
              <tr>
                <td>
                   <div class="main-title multilang" lang-key="Common.AppTitle" >博威APP</div>
                </td>
                <td width="5%">
                   <!-- <button type="button" onclick="reloadiframe()">Reload</button>-->
                </td>
        
                    <td width="5%" align="left">
                       <a href="MainMenu.htm?t=4" data-iconpos="notext" data-role="button" data-icon="gear"></a>
                         
                    </td>
                </tr>
            </table>
        </div> 

        <div data-role="content" data-theme="c">
            <a href="Controls/MyActivity.htm?t=<%=salt %>" data-role="button" class="ui-btn ui-icon-carat-r ui-btn-icon-right"
                data-transition="slide"><span class="multilang" lang-key="index.MyActivity">簽核待辦事項</span></a> 
            <a href="Controls/MyActivity2.htm?t=<%=salt %>" data-role="button" data-theme="a"
                    class="ui-btn ui-icon-carat-r ui-btn-icon-right"><span class="multilang" lang-key="index.MyActivity2">專案任務待辦事項<span></span></a>
            <a href="Controls/SearchResult.htm?t=<%=salt %>"
                        data-role="button" class="ui-btn ui-icon-carat-r ui-btn-icon-right">
                        <span class="multilang" lang-key="index.ProjectList">專案進度</span></a>
        </div>
        <!-- #Include virtual="include/footer.inc" -->
        
        
    </div>
    <div data-role="popup" id="UserMenu" data-theme="d">
        <div data-role="controlgroup" data-type="horizontal">
            <a href="#" id="tel" data-role="button" data-icon="phone" data-theme="b" content="">
                phone</a> <a href="#" id="mail" data-role="button" data-icon="mail" data-theme="b"
                    content="">mail</a>
        </div>
    </div>
    
</body>
</html>
