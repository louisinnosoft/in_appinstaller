﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webconfig.aspx.cs" Inherits="bcsappweb.Setting.Webconfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../Scripts/jquery.mobile-1.4.4/jquery.mobile-1.4.4.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../Styles/default.css?t=<%=salt%>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../themes_1106/1106_LVTest.min.css" />
    <link rel="stylesheet" href="../themes_1106/jquery.mobile.icons.min.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div  data-role="page" >
    <div data-role="header" data-theme="c">
    <table width="100%">
              <tr>
                <td>
                   <div class="main-title multilang" lang-key="Common.AppTitle" 
                        style="background-color: #fcee21; border-color: #8a8212" >博威APP 網站系統設定</div>
                </td>
                <td width="5%">
                   <!-- <button type="button" onclick="reloadiframe()">Reload</button>-->
                </td>
        
                    <td width="5%" align="left">
                       
                         
                    </td>
                </tr>
            </table>
                </div>
                <div data-role="content" >
        <table >
         <tr>
                <td>
                    靜態檔案版本
                </td>
                <td>
                    <asp:TextBox ID="txt_salt" runat="server" Width="66px"></asp:TextBox>
                </td>
                
            </tr>
            <tr>
                <td>
                    預設語系
                </td>
                <td>
                    <asp:DropDownList ID="ddl_default_lang" runat="server">
                        <asp:ListItem Text="繁體中文" Value="zt"></asp:ListItem>
                        <asp:ListItem Text="簡體中文" Value="zc"></asp:ListItem>
                        <asp:ListItem Text="英文" Value="en"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                
            </tr>
            
            <tr>
                <td valign=top>
                    Aras Innovator <br />連線資訊
                </td>
                <td>
                    <asp:TextBox ID="txt_url" runat="server" 
                        Width="360px" ToolTip="請輸入至: 'http://localhost/innovatorserver103' 層級即可" >http://localhost/innovatorserver103</asp:TextBox>
                    <br />                    
                    <asp:Label ID="Label1" runat="server" 
                        Text="(請輸入至: 'http://localhost/innovatorserver103' 層級即可)" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:Label ID="Label5" runat="server" Text="資料庫列表"></asp:Label>
                    <asp:DropDownList ID="ddlDB" runat="server">
                    </asp:DropDownList>
                    <asp:Button ID="btnGetDBList" runat="server" Text="取得資料庫列表" 
                        onclick="btnGetDBList_Click" />
                    <br /><asp:Panel ID="PLogin" runat="server" Visible="False">
                    
                    
                    <asp:Label ID="Label2" runat="server" Text="Label">帳號</asp:Label>
                    <asp:TextBox ID="txtLoginID" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="Label3" runat="server" Text="Label">密碼</asp:Label>
                    <asp:TextBox ID="txtPWD" runat="server" TextMode="Password" 
                        ViewStateMode="Enabled"></asp:TextBox>
                    <asp:Button ID="btnLogin" runat="server" Text="登入" 
                        onclick="btnLogin_Click" />
                        </asp:Panel>
                    <br /><asp:Panel ID="PFile" runat="server" Visible="False">
                    
                    
                    <asp:Label ID="Label4" runat="server" Text="Label">檔案列表</asp:Label>
                    <asp:DropDownList ID="ddlFileList" runat="server">
                    </asp:DropDownList>
                    
                    <asp:Button ID="btnMoveFile" runat="server" Text="搬移檔案" 
                        onclick="btnMoveFile_Click" />
                    <br />
                    <asp:HyperLink ID="hlDownloadFile" runat="server" Target="_blank"></asp:HyperLink>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
            <td colspan=2>
                    <asp:Button ID="submit" runat="server" Text="確認變更" onclick="submit_Click" />

            </td>
            </tr>
            <tr>
            <td colspan=2>
            <asp:Button ID="btnClearMessage" runat="server" Text="清除Log" 
                    onclick="btnClearMessage_Click" />
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                
            </td>

            </tr>
            
           
        </table>
        </div>
    </div>
    </form>
</body>
</html>
